## Group 38 Track My Brain- Capturing User's Own Reflections


### Contact Details
 - Supervisor: Muhit Anik - muhit.anik@sydney.edu.au
 - Client: Ben Sand - ben+sydney2019@bensand.com
 - Group Member:
       * Peter Liang	peter.liang.official@gmail.com 0451033778
       * Kevin Su	kevinsu@outlook.co.nz	0420426162
       * Lucy Chang	Lucalucy0220@gmail.com 0452022079
       * Iris Cong	rcon3244@gmail.com	0452596604
       
### System Diagram Explaination
 -  ***popup.js***  and ***popup3.js*** have function ***downloadContent()*** in order to put the answers of those questions that were asked in ***prepare.html*** and ***brief.html*** into a word doc file and download it to local machine  
 -  After the button in ***project.html*** is clicked, it navigates to ***popup2.js*** script. In ***popup2.js***, there are two functions: ***init()*** is loaded automatically when the content is loaded, and ***timeButttonOnClickHandler(event)*** which is triggered when any of the time-group button on ***project.html*** is selected. 
 -  After the button on ***project.html*** is clicked, it navigates to ***timer.html*** page. In this page, it runs the script of ***moment.js***, ***popupTracker.js***, and ***timer.js***. ***time.js*** implements the first section, countdown timer, on this page. ***popupTracker.js*** is the script running for cursor and scroll tracking. ***moment.js*** is the open source for timer.   
 -  After the user decides to finish tracking on the previous page, it navigates to ***index.html***. On this page, there are many different sections that link to different pages, ***social_networking.html***, ***readingandreference.html***, ***others.html***, ***creativity.html***, ***productivity.html***, ***entertainment.html***. For this page, it runs the script of ***index.js*** which contains the main functions to create bar chart. 
 -  After users are done with reviewing their productivity reports, they will be guided into debrief page ***brief.html*** to reflect on what they've done during the work cycles.  

 
### [WIKI](https://bitbucket.org/peterliang/track-my-brain-capturing-users-own-reflections/wiki/Home) 

You may find everything helpful and related to our project in [WIKI](https://bitbucket.org/peterliang/track-my-brain-capturing-users-own-reflections/wiki/Home) page. 

### Overview of Group Contribution

**Name**|**Group Report Contribution**
-----|-----
Peter Liang (Jianhui)|Report - Executive Summary
 |Report - Introduction
 |Report - Overview of System from User View
 |Technical - Chrome Timer Mockup
 |Technical - UI Design of the Chrome Extension
 |Technical - Help integrating Clickstream & Viewstream team’s project
 |Technical - Testing Setup
 |Non-Technical - Solo Presentation to Client and Investors
 |Non-Technical - Communication with Client
 |Non-Technical - Giving Direction to the Team & Holding members accountable
Lucy Chang|Report - Information Search
 |Report - Group Reflections and Conclusions
 |Technical - Preparation and Debrief page
 |Technical - Integration from scroll tracking
 |Technical - Help with setting user-defined length buttons
 |Technical - Code comments for every javascript and HTML file
 |Technical - Testings 
 |Non-Technical - preparing for a demo video and presentation slides
 |Non-Technical - managing WIKI page 
 |Non-Technical - managing issues and other aspects on bitbucket
Iris Cong|Report - System structure overview
 |Report - Tools used to build system
 |Technical - automated buttons
 |Technical - automated-test for line graph
 |Technical - Chrome Timer
 |Technical - energy and moral line graph
 |Group Process - Trello
 |Non-Technical - use case diagram
 |Non-Technical - improvement part for demo
Kevin Su|User story testing, software testing, usability testing.
 |Most of the HTML for the Chrome Extension and some of the JavaScript.
 |Held meeting minutes.