/**  removeEventListener.js
 * get the intervalId stored in chrome local storage by 'addEventListener.js',
 * clear the interval and that "recording" function will stop executing
 */
var intervalId;
var getDataIntervalId;
chrome.storage.local.get(["intervalId"], function(result) {
  intervalId = result.intervalId;
});
clearInterval(intervalId);

chrome.storage.local.get(["getDataIntervalId"], function(result) {
  getDataIntervalId = result.getDataIntervalId;
});
clearInterval(getDataIntervalId);


//NEWLY ADDED

var intervalIdScroll;
var scrollIntervalId;
chrome.storage.local.get(["intervalIdScroll"], function(result) {
  intervalIdScroll = result.intervalIdScroll;
});
clearInterval(intervalIdScroll);

chrome.storage.local.get(["scrollIntervalId"], function(result) {
  scrollIntervalId = result.scrollIntervalId;
});
clearInterval(scrollIntervalId);