/** stop_heatmap.js
 * The event listener to stop tracking when the button is clicked.
 */
$(document).ready(function() {
    $("#stop").click(function(){
        $("#hide-span").trigger('click'); 
    }); 
})