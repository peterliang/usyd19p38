/** time.js
 * Contain the functions to update the current time.
 */
function updateClock() {
    var now = new Date(); // current date
    var hours = now.getHours();
    if(hours < 10){
        hours = '0' + hours;
    }
    var mins = now.getMinutes();
    if(mins < 10){
        mins = '0' + mins;
    }
    var sec = now.getSeconds();
    if(sec < 10){
        sec = '0' + sec;
    }

    time = "Current Time is " + hours + ':' + mins + ':' + sec; // again, you get the idea

    // set the content of the element with the ID time to the formatted string
    document.getElementById('time').innerHTML = time;

    // call this function again in 1000ms
    setTimeout(updateClock, 1000);
}
updateClock(); // initial call