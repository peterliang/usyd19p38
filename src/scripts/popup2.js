/** popup2.js
 * This script for project.html.
 */

function init() {
    localStorage["pomodoro-selection"] = 20*60;
    localStorage["break-selection"] = 10*60;
    localStorage["cycle-selection"] = 1;

	document.getElementById("pomodoro").innerText = parseInt(localStorage["pomodoro-selection"])/60|| 10;
	document.getElementById("break").innerText = parseInt(localStorage["break-selection"])/60 || 10;
    document.getElementById("cycle").innerText = localStorage["cycle-selection"] || 1;
    localStorage["current-cycles"] = 1;
    localStorage["total-time"] = parseInt(localStorage["cycle-selection"]) * parseInt(localStorage["pomodoro-selection"]);
	var buttonGroups = document.getElementsByClassName("time-buttons-group")
	Array.prototype.forEach.call(buttonGroups, function(divElem) {

		Array.prototype.forEach.call(divElem.childNodes, function(elem) {
			elem.onclick = timeButtonOnClickHandler;
		});

	});

}

/**
 * Add click handlers to settings for user.
 */

function timeButtonOnClickHandler(event) {
	var targetElem = event.target;
	var timeSelected = targetElem.innerText; // Get button text and convert to number
	var settingKey = targetElem.parentNode.id; // Get id of node (tells us whether break or pomodoro)

	if(timeSelected == "+"){
	    if(settingKey == "cycle-selection"){
	        var update = parseInt(localStorage[settingKey]) + 1;
	        if(update > 4){
	            localStorage[settingKey] = 4;
                document.getElementById(settingKey.split("-")[0]).innerText = localStorage[settingKey];
	        }else{
	            localStorage[settingKey] = parseInt(localStorage[settingKey]) + 1;
                document.getElementById(settingKey.split("-")[0]).innerText = localStorage[settingKey];
	        }
	    }else{
	        var update = parseInt(localStorage[settingKey]) + 300;
	        if(update > 3600){
	            localStorage[settingKey] = 3600;
	            document.getElementById(settingKey.split("-")[0]).innerText = localStorage[settingKey]/60;
	        }else{
	            localStorage[settingKey] = parseInt(localStorage[settingKey]) + 300;
                document.getElementById(settingKey.split("-")[0]).innerText = localStorage[settingKey]/60;
	        }

	    }

	}else if(timeSelected == "-"){
	    if(settingKey == "cycle-selection"){
	        var update = parseInt(localStorage[settingKey]) - 1;
	        if(update < 1){
	            localStorage[settingKey] = 1;
                document.getElementById(settingKey.split("-")[0]).innerText = localStorage[settingKey];
	        }else{
	            localStorage[settingKey] = parseInt(localStorage[settingKey]) - 1;
                document.getElementById(settingKey.split("-")[0]).innerText = localStorage[settingKey];
	        }
        }else{
            var update = parseInt(localStorage[settingKey]) - 300;
            if(update < 300){
                localStorage[settingKey] = 300;
                document.getElementById(settingKey.split("-")[0]).innerText = localStorage[settingKey]/60;
            }else{
                localStorage[settingKey] = parseInt(localStorage[settingKey]) - 300;
                document.getElementById(settingKey.split("-")[0]).innerText = localStorage[settingKey]/60;
            }
        }
	}
}
document.addEventListener('DOMContentLoaded', init);
