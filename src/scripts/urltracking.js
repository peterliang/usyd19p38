var arr = []

function start(){
    chrome.tabs.query({'active': true, 'lastFocusedWindow': true}, function (tabs) {
        var tab = tabs[0];
        var url2 = new URL(tab.url);
        var domain = url2.hostname;
        if(arr.length == 0){
            arr.push(domain);
        }else{
            var alreadyIn = false;
            for(var i = 0; i < arr.length; i++){
                if(arr[i] == domain){
                    alreadyIn = true;
                }
            }
            if(alreadyIn == false){
                arr.push(domain);
            }
        }
        document.getElementById("url").innerHTML = "Website: " + arr;
    });
}

start()


