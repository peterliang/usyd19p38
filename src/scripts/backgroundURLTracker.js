/**  backgroundURLTracker.js
 * Contain function to get the current url that users are using.
 * If domain changes, recording will pause until manually resume
 */
var urlSecArray = new Array;
var beginVTArray = new Array; //last visit time of each website
var tabID = "0";
var visitTime = new Date();
var oldVisitTime;
var timeArray = new Array;
var currentID;
var ua = new Array;

chrome.storage.local.get({ua: []}, function(data){
	if(typeof data.ua === 'undefined'){
		chrome.storage.local.set({ua: ua});
	}
})

function getDomain(url){
	var domain = url.split(".");
	return domain[1];
}
chrome.tabs.onUpdated.addListener( function (tabID, tabStatus, tab){
	if(tabStatus.status == 'complete'){
		var url = tab.url;

		var cleanURL;
		//get URL, clean URL and match against unproductive list
		cleanURL = sanitiseURL(url);
		var domain = cleanURL.split(".");

		addLastVisitTime(tab, cleanURL);

		chrome.storage.local.get({unprodArray:[]}, function(data){
			for(var i=0, length=data.unprodArray.length; i<length; i++){
				if(domain[1] === data.unprodArray[i] || domain[0] === data.unprodArray[i]){
					alert("Unproductive site!");
				}
			}
		})
	}
});

//check which tab (site) is in focus and how long on the site
chrome.tabs.onActivated.addListener(function (data){
	chrome.tabs.get(data.tabId, function(tab){
		shortURL = sanitiseURL(tab.url);

		setTimeout(function(){
			currentID = tab.id + "" +tab.windowId;
			//alert(currentID);
		}, 500);



		//if first tab visited: set the first visit time stamp
		if(tabID === "0"){
			oldVisitTime = visitTime.getTime();
			timeArray.push(oldVisitTime);
		}
		//if not first tab visited: run changed focused tab fn
		if(tabID != "0"){	actChanged(tabID, timeArray); }

		//keeping record of last visited time for each website, used when tab is closed
		addLastVisitTime(tab, shortURL);
		//update global tabid to current tabid
		tabID = data.tabId;
	})
});
//Update time if user changes tab in focus without updating or closing the tab
function actChanged(tabID, timeArray){
	chrome.tabs.get(tabID, function(tab){
		if(chrome.runtime.lastError){
			console.log(chrome.runtime.lastError.message);
		} else {
			newURL = sanitiseURL(tab.url);
			//calculate time spent on this tab (current time minus time began vist)
			var newVT = new Date();
			var newTime = newVT.getTime();
			var sec = sanitiseTime(timeArray[0], newTime);

			//fn for updating the data to be displayed
			updateTimeVisited(newURL, sec, newVT);

			timeArray[0] = newTime;
		}
	})
}

//update time spent on the tab (site) when it is closed
chrome.tabs.onRemoved.addListener(function(tabID, info){

		var tempTime = new Date();
		var currentTime = tempTime.getTime();
		var uniqueID = tabID + "" + info.windowId;

		for(var i=0; i<beginVTArray.length; i++){
			if(beginVTArray[i][2] === uniqueID){
				var tempURL = beginVTArray[i][0];
				var sec = sanitiseTime(beginVTArray[i][1], currentTime);

				if(beginVTArray[i][2] === currentID){
					updateTimeVisited(tempURL, sec, tempTime);
				}
			}
		}


})



function padding(n){
	if(n < 10){
		n = '0'+n;
	}
	return n;
}
function updateTimeVisited(newURL, sec, newTime){
	if(newURL != "chrome-extension:"){
			if( newURL != "chrome:"){

				var urlSec = new Array;
				var n = 0;
				var month = newTime.getMonth();
				var minute = newTime.getMinutes();
				minute = padding(minute);

				var second = newTime.getSeconds();
				second = padding(second);

				var time = newTime.getDate() + "/" + (month+1) + "/" + newTime.getFullYear() + " " + newTime.getHours() + ":" + minute + ":" + second;
				var xArray = new Array;


				chrome.storage.local.get({urlArray:[]}, function(data){
					if(typeof data.urlArray != 'undefined'){
						urlSecArray = data.urlArray;
					}

					for(var i=0, length=urlSecArray.length; i<length; i++){
						if(urlSecArray[i][0] === newURL){
							urlSecArray[i][1] = sec + urlSecArray[i][1];
							urlSecArray[i][2] = time;
							n = 1;

							chrome.storage.local.set({urlArray: urlSecArray}, function(){
							})

							break;
						}
					}



					if(n === 0) {
							urlSec.push(newURL);
							urlSec.push(sec);
							urlSec.push(time);
							urlSecArray.push(urlSec);
							chrome.storage.local.set({urlArray: urlSecArray}, function(){
							})
					}
				})

			}
	}
}



function addLastVisitTime(tab, shortURL){
	var lastVisitTime = new Date();
	var n = 0;
	for(var i=0; i<beginVTArray.length; i++){
		if(beginVTArray[i][0] === shortURL){
			beginVTArray[i][1] = lastVisitTime.getTime();
			beginVTArray[i][2] = tab.id + "" + tab.windowId;
			n = 1;
			break;
		}
	}
	if(n === 0){
		if(shortURL != "chrome:"){
			var temp = new Array;
			temp.push(shortURL);
			temp.push(lastVisitTime.getTime());
			temp.push(tab.id + "" + tab.windowId);
			beginVTArray.push(temp);
		}
	}
}

function sanitiseURL(url){

      var urlArray = url.split("/");
      var cleanURL;

      if(urlArray.length == 1){
            cleanURL = url;
      }else{
            for(var i = 0; i < urlArray.length; i++){
                  if(urlArray[i] != "https:" && urlArray[i] != ""){
                        cleanURL = urlArray[i];
                        return cleanURL
                  }
            }
      }
      return cleanURL;
};

function sanitiseTime(oldTime, newTime){

  var difference = (newTime - oldTime)/1000;

  return Math.abs(Math.round(difference));
}
