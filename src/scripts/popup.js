/** popup.js
 * This script for popup.html.
 */
document.addEventListener('DOMContentLoaded', function () {
    document.getElementById("submit").addEventListener("click", downloadContent);
});

function downloadContent() {
  content = getContent();
  var atag = document.createElement("a");
  var file = new Blob([content], { type: 'text/plain' });
  atag.href = URL.createObjectURL(file);
  var datetime = new Date().toLocaleString()
  atag.download = "WorkCycle_"+datetime+".doc";
  atag.click();
}
module.exports = downloadContent;

function getContent() {
  var q1 = "1. What am I trying to accomplish?\n" + "  " + document.getElementById("Q1").value;
  var q2 = "2. Why is this important and valuable?\n" + "     " + document.getElementById("Q2").value;
  var q3 = "3. How will I know this is complete?\n" + "    " + document.getElementById("Q3").value;
  var q4 = "4. Any risks / hazards? Potential distractions, procrastination, etc.\n" + "   " + document.getElementById("Q4").value;
  var q5 = "5. Is this concrete / measurable or subjective / ambiguous?\n" + "     " + document.getElementById("Q5").value;
  var q6 = "6. Anything else noteworthy?\n" + "   " + document.getElementById("Q6").value;
  
  var content = "Preparation for the work cycle" + "\n" + "\n" + q1 + "\n" + q2 + "\n" + q3 + "\n" + q4 + "\n" + q5 + "\n" + q6;
  return content;
}
module.exports = getContent;


function sum(a, b) {
  return a + b;
}
module.exports = sum;
