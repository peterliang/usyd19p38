/**  auto_click.js
 * Helper function that links to start function when the button is clicked.
 */

$(document).ready(function(){
    $("#start").trigger('click'); 
});
