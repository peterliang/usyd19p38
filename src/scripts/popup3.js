/** popup3.js
 * This script for brief.html.
 */
document.addEventListener('DOMContentLoaded', function () {
    document.getElementById("submit").addEventListener("click", downloadContent);
});

function downloadContent() {
  var q1 = "1. What did I get done this period?\n" + "    " + document.getElementById("Q1").value;
  var q2 = "2. How does this compare to normal?\n" + "    " + document.getElementById("Q2").value;
  var q3 = "3. Did I get bogged down? Where?\n" + "    " + document.getElementById("Q3").value;
  var q4 = "4. What went well? How can I recreate this?\n" + "    " + document.getElementById("Q4").value;
  var q5 = "5. Any other lessons learnt?\n" + "    " + document.getElementById("Q5").value;

  var content = "Reflection for the work cycle" + "\n" + "\n" + q1 + "\n" + q2 + "\n" + q3 + "\n" + q4 + "\n" + q5;
  var atag = document.createElement("a");
  var file = new Blob([content], { type: 'text/plain' });
  atag.href = URL.createObjectURL(file);
  var datetime = new Date().toLocaleString()
  atag.download = "WorkCycle_"+datetime+".doc";
  atag.click();
}
module.exports = downloadContent;


function sum(a, b) {
  return a + b;
}
module.exports = sum;
