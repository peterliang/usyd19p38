/** timer.js
 * Initially called when extension page loads. Sets up a bunch of stuff..
 */
function init() {
	addOnClick();
	addMessageListeners();
	startTimer();
}

/**
 * Sends a message to background page to start the timer.
 */
function startTimer() {
	chrome.runtime.sendMessage({
		"command": "startTimer"
	}, function(response) {
		console.log(response.message);
	});
}

/**
 * Adds listeners so it knows how to handle the messages from the background page.
 */
function addMessageListeners() {
	chrome.runtime.onMessage.addListener(function(request, sender, sendResponse) {
		switch(request.command) {
			case "updateTime":

			    /** Our modified code to display the amount of time remaining (more in background.js **/
			    var timeState = request.time;
			    var currentTime = timeState[0];
			    var endTime = timeState[1];
			    var cycles = localStorage["cycle-selection"];
                var currentCycle = localStorage["current-cycles"];

                //** manually formatting the total time **/

                var total = moment().startOf("day").seconds(localStorage["total-time"]).format("h:mm:ss")

                document.getElementById("show-cycles").innerText = "Cycle " + currentCycle + " out of " + cycles;
				document.getElementById("current-time").innerText = currentTime;
				document.getElementById("end-time").innerText = endTime;
				document.getElementById("break").innerText = "until break"
				document.getElementById("total-time").innerText = total;
				break;
			case "timerEnded":
				console.log("Timer ended.");
				break;
		}
	});
}

/**
 * Adds onclick listener to the stop button.
 */
function addOnClick() {
	document.getElementById("stop").onclick = function() {
		chrome.runtime.sendMessage({
			"command": "endTimer"
		});
		document.location = chrome.runtime.getURL("brief.html");
		chrome.browserAction.setBadgeText({"text" : ""});
	}
}

document.addEventListener('DOMContentLoaded', init);
