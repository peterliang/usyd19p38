/**  addEventListener.js
 * This file contains functions for scroll map features.
 * These helper functions will get the scroll position and mouse position,
 * and updated them when needed.
 */

//define two variables to store mouse position
var positionX;
var positionY;

var windowPositionX;
var windowPositionY;

//listen mouse event
document.onmousemove = function(e) {
  //get the mouse position in fully rendered content area in the browser
  windowPositionX = e.clientX;
  windowPositionY = e.clientY;
  var x = e.clientX + pageXOffset;
  var y = e.clientY + pageYOffset;
  positionX = x > document.documentElement.scrollWidth ? document.documentElement.scrollWidth : x;
  positionY = y > document.documentElement.scrollHeight ? document.documentElement.scrollHeight : y;
  // positionX = e.clientX;
  // positionY = e.clientY;
};

function getElementData(){
  var img = document.elementFromPoint(windowPositionX, windowPositionY).getElementsByTagName('img');
  if(img != null){
    console.log(img)
  } else {
    console.log('no image element found');
  }
}

//use following flag to track whether mouse is currently in the content area,
//will pause recording if mouse leaves this area and resume when it comes back
var mouseIsInBrowsingArea = true;
document.onmouseenter = function(e) {
  mouseIsInBrowsingArea = true;
};
document.onmouseleave = function(e) {
  mouseIsInBrowsingArea = false;
};

//store mouse position in chrome local storage as an array
function getNewMousePosition() {
  //don't record if mouse is not in content area
  if (!mouseIsInBrowsingArea || !positionX || !positionY) {
    console.log('not recording data');
    return;
  }
  //represent current mouse position as an array with a length of 2
  // console.log(positionX, Math.round(positionY));
  var newPointArray = [positionX, Math.round(positionY)];
  //will store all positions we get in chrome local storage as key value pair,
  //key is "mouseMovementArray", value is a two dimensional array,
  //each time we get a position, we push it into this big array
  //if it is the very first position we've got, we will need to create this array
  chrome.storage.local.get(["mouseMovementArray"], function(result) {
    if (
      typeof result.mouseMovementArray !== "undefined" &&
      result.mouseMovementArray instanceof Array
    ) {
      //not the first point, just push it into the array
      result.mouseMovementArray.push(newPointArray);
    } else {
      //this is the first point we got, put it in an array and assign it to result.mouseMovementArray
      result.mouseMovementArray = [newPointArray];
    }
    //store the modified array back to chrome local storage
    chrome.storage.local.set({ mouseMovementArray: result.mouseMovementArray });
  });
}

//set an interval of 0.1s to continuously execute this function to get positions
var intervalId = setInterval(getNewMousePosition, 100);
var getDataIntervalId = setInterval(getElementData, 1000);
//store the interval id in local storage, will need to clear the interval through this id when needed
chrome.storage.local.set({ intervalId: intervalId });
chrome.storage.local.set({ getDataIntervalId: getDataIntervalId})

// NEWLY ADDED
function getScrollPosition() {
  // //don't record if mouse is not in content area
  if (!mouseIsInBrowsingArea) {
    return;
  }
  chrome.storage.local.get(["scrollArray"], function(result){
    if(
      typeof result.scrollArray !== "undefined" && 
      result.scrollArray instanceof Array
    ) {
        let top = Math.floor(window.pageYOffset/100);
        let bottom = top + Math.ceil(document.documentElement.clientHeight/100);
        for(var i = top; i < bottom; i++) result.scrollArray[i]++
        // console.log(result.scrollArray)
      
    } else {
      console.log('creating array')
      result.scrollArray = [];
      for(var i = 0; i < Math.ceil(document.documentElement.scrollHeight/100) ; i++) result.scrollArray[i] = 0;
      // console.log(document.documentElement.scrollHeight/100)
    }

    chrome.storage.local.set({ scrollArray: result.scrollArray });

  })
}

//set an interval of 0.1s to continuously execute this function to get positions
var intervalIdScroll = setInterval(getNewMousePosition, 100);
var scrollIntervalId = setInterval(getScrollPosition, 500)
//store the interval id in local storage, will need to clear the interval through this id when needed
chrome.storage.local.set({ intervalIdScroll: intervalIdScroll });
chrome.storage.local.set({ scrollIntervalId: scrollIntervalId})