/** produce_line.js
 * Contain functions that store cycle and data.
 */
//store the energy and morale level for each cycle
var store_level = document.getElementById("this_button");
if (store_level) {
    store_level.addEventListener("click", storeData);
}

//store the cycle number
var store_cycle = document.getElementById("my_button");
if (store_cycle) {
    store_cycle.addEventListener("click", storeCycle);
}

//show the line graph
var show_graph = document.getElementById("show_graph");
if (show_graph) {
	show_graph.addEventListener("click", showGraph);
    show_graph.click();
}



var pomodoro=0;
var break_var=0;
var cycle;
var counter=0;
var energy=[];
var morale=[];
var this_counter=0;

function storeCycle() {
	// dont need right now, keep for further usage
	pomodoro = document.getElementById("pomodoro").innerText = localStorage["pomodoro-selection"]/60;
	break_var = document.getElementById("break").innerText = localStorage["break-selection"]/60;
	
	//get the cycle number and store it in local, prevent it becomes undefined when changing heml page
	var cycle_string =  document.getElementById("cycle").innerText;
	cycle = parseInt(cycle_string, 10);
	localStorage.setItem("cycle_number", cycle);
}

function storeData() {
	//get the cycle number previously stored in local storage
	cycle = localStorage.getItem("cycle_number");
	console.log(cycle);
	

	
	//store them into the array
	var this_energy = document.getElementById("energy");
	var this_energy = this_energy.options[this_energy.selectedIndex].value;
	
	var this_morale = document.getElementById("morale");
	var this_morale = this_morale.options[this_morale.selectedIndex].value;
	
	if(counter<cycle){
		if(this_energy==='high'){
			energy.push(1000);
		}else if(this_energy==='medium'){
			energy.push(666);
		}else if(this_energy==='low'){
			energy.push(333);
		}
		if(this_morale==='high'){
			morale.push(1000);
		}else if(this_morale==='medium'){
			morale.push(666);
		}else if(this_morale==='low'){
			morale.push(333);
		}
		if(counter<cycle-1){
			var new_counter = (counter+2).toString();
			var cycle_string = "Cycle ";
			var new_string = cycle_string.concat(new_counter);
			document.getElementById("cycle_no").innerHTML = new_string;
		}else{
			chrome.tabs.create({url: chrome.extension.getURL('final_graph.html')});
			}
			console.log(counter);
			console.log(energy);
			console.log(morale);
	}
	counter++;
	
	if(counter == cycle){
		var energy_JSON = JSON.stringify(energy);
		localStorage.setItem("energy_level", energy_JSON);
		var morale_JSON = JSON.stringify(morale);
		localStorage.setItem("morale_level", morale_JSON);
	}
}

function showGraph() {
	
	var energy_JSON = localStorage.getItem("energy_level");
	energy = JSON.parse(energy_JSON);
	
	var morale_JSON = localStorage.getItem("morale_level");
	morale = JSON.parse(morale_JSON);
	
	console.log(energy);
	console.log(morale);
	
	
	var cycle_arr=["Cycle 1", "Cycle 2", "Cycle 3", "Cycle 4"];
	//prepare the dataset
	var data = {
	  labels: cycle_arr,
	  datasets: []
	};
	
	var options = {
			title: {
			  display: true,
			  text: 'Energy & Morale Graph'
			}
		  }


	var myEnergyDataset = {
			label:"energy",
			data:energy,
			borderColor: "#3e95cd",
			fill: false
	}
	
	
	var myMoraleDataset = {
			label:"morale",
			data:morale,
			borderColor: "#8e5ea2",
			fill: false
	}
	
	
	var ctx = document.getElementById('line_chart');
	

	var myChart = new Chart(ctx, {type:'line', data: data, options: options});
	new Chart(document.getElementById("line_chart"), {
	  type: 'line',
	  data: {
		labels: cycle_arr,
		datasets: [myEnergyDataset, myMoraleDataset]
	  },
	  options: {
		title: {
		  display: true,
		  text: 'Energy & Morale Line Graph'
		}
	  }
	});

		
		
}
	
	
	
	
	
	






 