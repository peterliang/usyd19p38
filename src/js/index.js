$("#switch").change(function () {
    if ($("input[type='checkbox']").is(':checked') == true) {
    } else {
    }
});
$(".menuListUl li").click(function () {
    $(this).addClass("menuSelect");
    $(this).siblings().removeClass("menuSelect");
});

function today() {
    $("#weekdayButton").removeClass("selectDay").addClass("defaultDay");
    $("#todayButton").removeClass("defaultDay").addClass("selectDay");
    $("#showChartDiv").show();
    $("#showTimeDiv").show();
}

function weekday() {
    $("#todayButton").removeClass("selectDay").addClass("defaultDay");
    $("#weekdayButton").removeClass("defaultDay").addClass("selectDay");
    $("#showTimeDiv").hide();
}
$(function () {
    //Functions for productivity score
    let s_prod = 100, s_ent = 0, s_sn = 30, s_c = 65, s_rr = 80, s_other = 0;
    //DUMMY DATA IN JSON STYLE
    let dataList =  [{prod:6400},{ent:300},{sn:6000},{crea:600},{rr:500},{other:900}];
    let totalVal = 0;
    let p = 0, e = 0, s = 0, c = 0, r = 0, o = 0;
    dataList.forEach(val => {
        let key = Object.keys(val);
        let value = parseInt(Object.values(val));
        if(key == "prod")
        {
            p = value;
        }
        if(key == "ent")
        {
            e = value;
        }
        if(key == "sn")
        {
            s = value;
        }
        if(key == "crea")
        {
            c = value;
        }
        if(key == "rr")
        {
            r = value;
        }
        if(key == "other")
        {
            o = value;
        }

        totalVal += value;
    });
    let p_Percentage = p / totalVal, e_Percentage = e /totalVal, s_Percentage = s / totalVal, c_Percentage = c / totalVal, r_Percentage = r / totalVal, o_Percentage = o / totalVal;
    let score = p_Percentage * s_prod + e_Percentage * s_ent + s_Percentage * s_sn + c_Percentage * s_c + r_Percentage * s_rr + o_Percentage * s_other;
    debugger;
    $("#score").text(score.toFixed(2));
});




require.config({
    paths: {
        echarts: 'js/echarts/dist'
    }
});
// use echarts
require(
    [
        'echarts',
        'echarts/chart/bar', // 使用柱状图就加载bar模块，按需加载
        'echarts/chart/pie',
        'echarts/chart/line'
    ],
    function (ec) {
        // bar chart

        var barChart = ec.init(document.getElementById('barChart'));

        //convert ARRAY TO jason
        const testArray = [["Productivity", 86400], ["Entertainment", 300], ["SocialNetwork", 60000], ["Creativity", 600], ["ReadingandReference", 500], ["Others", 900]];


        // JSON 数据类型
        //const barDataList = [{Productivity: 86400}, {Entertainment: 300}, {SocialNetwork: 60000}, {Creativity: 600}, {ReadingReference: 500}, {Others: 900}];

        //load data to bar chart using array.
        const data = [];
        const value = [];
        for (var i = 0; i < testArray.length; i++) {
            data[i] = testArray[i][0];
            let val = testArray[i][1] / 60 / 60;
            value[i] = val.toFixed(2);
        }

        let barChartData = data;
        let barChartValue = value;


        //for(let barData of barDataList)
        //{
        //    barChartData.push(Object.keys(barData));
        //    let val = Object.values(barData);
        //    val = val/60/60;
        //    barChartValue.push(val.toFixed(1));
        //}
        // show the data and value used in bar chart
        console.warn(barChartData);
        console.warn(barChartValue);
        var barOption = {
            grid: { // 控制图的大小，调整下面这些值就可以，
                x: 100,//控制x轴文字与底部的距离
                y2: 200 // y2可以控制倾斜的文字狱最右边的距离，放置倾斜的文字超过显示区域
            },
            tooltip: {
                show: true
            },
            xAxis: [
                {
                    type: 'category',
                    data: barChartData,
                    axisLabel: {
                        interval: 0,    //强制文字产生间隔
                        rotate: 0,     //文字逆时针旋转45°
                        textStyle: {    //文字样式
                            color: "black",
                            fontSize: 12,
                            fontFamily: 'Microsoft YaHei'
                        }
                    }
                }
            ],
            yAxis: [
                {
                    type: 'value'
                },

            ],
            series: [
                {
                    "type": "bar",
                    "data": barChartValue,
                    itemStyle: {
                        normal: {
                            label: {
                                show: true,
                                position: 'top',
                                textStyle: {
                                    color: 'black',
                                    fontSize: 12
                                }
                            }
                        }
                    },
                    formatter : function(val)
                    {
                        return Math.round(val);
                    }
                }
            ]
        };

        // load data for echarts
        barChart.setOption(barOption);
        //end of bar chart

        //pie chart start here
        var pieChart = ec.init(document.getElementById('pieChart'));

        var pieDataList =  [{Productivity:86400},{Entertainment:300},{SocialNetwork:60000},{Creativity:600},{ReadingReference:500},{Game:500},{Others:900}];
        let pieChartData = [];
        let pieChartValue = [];


        for(let pieData of pieDataList)
        {
            let key = Object.keys(pieData);
            let val = Object.values(pieData);
            val = val/60/60;
            val = val.toFixed(1);
            pieChartData.push(key);
            // fail to use array coz pie chart requires 2 attributes for VALUE.
            pieChartValue.push({value:val,name:key});
        }

        console.warn(pieChartData);
        console.warn(pieChartValue);
        var pieOption = {
            title : {
                text: 'Percentage of each categories',
                x:'center'
            },
            tooltip : {
                trigger: 'item',
                formatter: "{a} <br/>{b} : {c} ({d}%)"
            },
            legend: {
                orient : 'vertical',
                x : 'left',
                data:pieChartData
            },
            calculable : true,
            series : [
                {
                    name:'',
                    type:'pie',
                    radius : '55%',
                    center: ['50%', '60%'],
                    data:pieChartValue
                }
            ]
        };
        // 为echarts对象加载数据
        pieChart.setOption(pieOption);
        //pie chart end

        //horizontal bar chart start

        const testdata2 = [["Facebook",300],["Reddit",500],["Google",700],["Yoututbe",6000],["Canvas",50000],["Bitbucket",6000],["Trello",5000],["Stackoverflow",4000]];
        var horizontalBarChart = ec.init(document.getElementById('horizontalBarChart'));

        /*chrome.storage.local.get({test: []}, function(data){
          alert(data.test);
        })*/

        var tempArray = new Array;

        chrome.storage.local.get({urlArray: []}, function(data){
            tempArray = data.urlArray;
            console.log(tempArray);
        //var horizontalBarDataList = [{facebook:300},{reddit:500},{google:700},{youtube:6000},{canvas:50000},{bitbucket:6000},{trello:5000},{stackoverflow:4000},{twitch:600},{carsales:500},{netflex:700},{seek:500}]
        //sort
        // horizontalBarDataList.sort(function (a,b) {
        //    return Object.values(b) - Object.values(a);
        //});
        debugger;

        tempArray = sortArr(tempArray);
        console.log(tempArray);

        if(tempArray.length > 0){

        var horizontalBarData = [];
        var horizontalBarValue = [];
        for(let i = 0, j=tempArray.length-1; i<tempArray.length && i < 10; i++, j--)
        {

              horizontalBarData[i]=tempArray[j][0];
              let val = tempArray[j][1];
              val = val.toFixed(2);
              horizontalBarValue[i]=val;


        }

        //horizontalBarData = arrayRemove(horizontalBarData, "chrome-extension:");
        //horizontalBarData = arrayRemove(horizontalBarData, "chrome:");

        console.log(horizontalBarData);

        console.warn(horizontalBarData);
        console.warn(horizontalBarValue);
        horizontalBarOption = {
            title : {
                text: 'Top 10 sites visited'
            },
            tooltip : {
                trigger: 'axis'
            },

            calculable : true,
            xAxis : [
                {
                    type : 'value',
                    boundaryGap : [0, 0.01]
                }
            ],
            yAxis : [
                {
                    type : 'category',
                    data : horizontalBarData
                }
            ],
            series : [
                {
                    name:'',
                    type:'bar',
                    data:horizontalBarValue
                }
            ]
        };



        horizontalBarChart.setOption(horizontalBarOption);
        //horizontal bar chart end
}
})

        //line graph start
        var lineChart = ec.init(document.getElementById('lineChart'));
        var lineDataList = [40,70,80,29,87,88,83,89,40,23];
        let lineChartData = [];
        let lineChartValue = [];
        for(let i = 0;i < lineDataList.length; i++)
        {
            lineChartData.push("Day"+(i+1));
            lineChartValue.push(lineDataList[i]);
        }
        let lineOption = {
            title : {
                text: 'Productivity score change'
            },
            tooltip : {
                trigger: 'axis'
            },

            calculable : true,
            xAxis : [
                {
                    type : 'category',
                    boundaryGap : false,
                    data : lineChartData
                }
            ],
            yAxis : [
                {
                    type : 'value',
                    axisLabel : {
                        formatter: '{value}'
                    }
                }
            ],
            series : [
                {
                    name:'Productivity score',
                    type:'line',
                    data:lineChartValue,
                    markPoint : {
                        data : [
                            {type : 'max', name: 'Maximum'},
                            {type : 'min', name: 'minimum'}
                        ]
                    }
                }
            ]
        };


        lineChart.setOption(lineOption);
        //line graph end

    }
);

function arrayRemove(array, value){
  return array.filter(function(element){
    return element != value;
  })
}

function sortArr(arr){

  var tempArr = new Array;
  for(var i=0; i<arr.length; i++){
    tempArr[i] = arr[i][1];
  }

  tempArr.sort(function(a,b){return b-a});

  var sortedArr = new Array;
  for(var j=0, k=1; j<tempArr.length && j<10; j++, k++){
    for(var i=0; i<arr.length; i++){
      if((tempArr[j] === arr[i][1]) && (sortedArr[k-1] != arr[i][0])){
        var temp = new Array;
        temp.push(arr[i][0]);
        temp.push(arr[i][1]);
        sortedArr.push(temp);
        arr = arrayRemove(arr, arr[i]);
        break;
      }
    }
  }

  return sortedArr;
}
