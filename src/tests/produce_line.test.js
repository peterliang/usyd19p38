/*
	Test for produce_line.js
	Written by Iris Cong
*/


/*
first run the whole program with the cycle number 3,
morale level:high medium high
enery level: medium high medium 
then there will be local stored cycle number
extract it from local storage to see if it matches
*/




/*
test cycle storage
*/
test('check if the cycle has been successfully stored', ()=>{
	var cycle = localStorage.getItem("cycle_number");
	expect(cycle).toBe(3);

}

/*
test energy storage
*/
test('check if the energy has been successfully stored', ()=>{
	var energy_copy = [666, 1000, 666]
	var energy_JSON = localStorage.getItem("energy_level");
	var energy = JSON.parse(energy_JSON);
	expect(energy).toBe(energy_copy);

}

/*
test morale storage
*/
test('check if the morale has been successfully stored', ()=>{
	var morale_copy = [1000, 666, 1000]
	var morale_JSON = localStorage.getItem("morale_level");
	var morale= JSON.parse(morale_JSON);
	expect(morale).toBe(morale_copy);
}



