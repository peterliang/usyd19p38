/*
    Test cases for popup.js
    Written By Lucy Chang
*/

/*
    Helper function
*/
function fullSentence (question, answer){
    return question + answer;
}

/*
    Test Question 1 on Prepare page
*/
function testQ1(){
    var question = "1. What am I trying to accomplish?\n";
    var answer = "Finish reviewing everything for ELEC.\n";
    return fullSentence(question, answer);
}
test('TEST Question 1 on Prepare Page', () =>{
    expect(testQ1()).toMatch("1. What am I trying to accomplish?\n" +  "Finish reviewing everything for ELEC.\n");
})

/*
    Test Question 2 on Prepare page
*/
function testQ2(){
    var question = "2. Why is this important and valuable?\n";
    var answer = "Because the final is coming\n";
    return fullSentence(question, answer);
}
test('TEST Question 2 on Prepare Page', () =>{
    expect(testQ2()).toMatch("2. Why is this important and valuable?\n" + "Because the final is coming\n");
})

/*
    Test Question 3 on Prepare page
*/
function testQ3(){
    var question = "3. How will I know this is complete?\n";
    var answer = "Complete the online quizzes.\n";
    return fullSentence(question, answer);
}
test('TEST Question 3 on Prepare Page', () =>{
    expect(testQ3()).toMatch("3. How will I know this is complete?\n" + "Complete the online quizzes.\n");
})

/*
    Test Question 4 on Prepare page
*/
function testQ4(){
    var question = "4. Any risks / hazards? Potential distractions, procrastination, etc.\n";
    var answer = "Chatting with friends might be the distraction.\n";
    return fullSentence(question, answer);
}
test('TEST Question 4 on Prepare Page', () =>{
    expect(testQ4()).toMatch("4. Any risks / hazards? Potential distractions, procrastination, etc.\n" +
                                "Chatting with friends might be the distraction.\n");
})

/*
    Test Question 5 on Prepare page
*/
function testQ5(){
    var question = "5. Is this concrete / measurable or subjective / ambiguous?\n";
    var answer = "Can count how many hours I've studied for.\n";
    return fullSentence(question, answer);
}
test('TEST Question 5 on Prepare Page', () =>{
    expect(testQ5()).toMatch("5. Is this concrete / measurable or subjective / ambiguous?\n" +
                                 "Can count how many hours I've studied for.\n");
})

/*
    Test Question 6 on Prepare page
*/
function testQ6(){
    var question = "6. Anything else noteworthy?\n";
    var answer = "Need to start reviewing for Final exam.\n";
    return fullSentence(question, answer);
}
test('TEST Question 6 on Prepare Page', () =>{
    expect(testQ6()).toMatch("6. Anything else noteworthy?\n" +  "Need to start reviewing for Final exam.\n");
})
