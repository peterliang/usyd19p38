/*
    Test cases for popup3.js
    Written By Lucy Chang
*/

/*
    Helper function
*/
function fullSentence (question, answer){
    return question + answer;
}

/*
    Test Question 1 on Prepare page
*/
function testQ1(){
    var question = "1. What did I get done this period?\n";
    var answer = "Finish group report for SOFT3888\n";
    return fullSentence(question, answer);
}
test('TEST Question 1 on Prepare Page', () =>{
    expect(testQ1()).toMatch("1. What did I get done this period?\n" + "Finish group report for SOFT3888\n");
})

/*
    Test Question 2 on Prepare page
*/
function testQ2(){
    var question = "2. How does this compare to normal?\n";
    var answer = "More efficient than usual\n";
    return fullSentence(question, answer);
}
test('TEST Question 2 on Prepare Page', () =>{
    expect(testQ2()).toMatch("2. How does this compare to normal?\n" + "More efficient than usual\n");
})

/*
    Test Question 3 on Prepare page
*/
function testQ3(){
    var question = "3. Did I get bogged down? Where?\n";
    var answer = "Chatting with friends.\n";
    return fullSentence(question, answer);
}
test('TEST Question 3 on Prepare Page', () =>{
    expect(testQ3()).toMatch("3. Did I get bogged down? Where?\n" + "Chatting with friends.\n");
})

/*
    Test Question 4 on Prepare page
*/
function testQ4(){
    var question = "4. What went well? How can I recreate this?\n";
    var answer = "Finish everything that is due on Friday.\n";
    return fullSentence(question, answer);
}
test('TEST Question 4 on Prepare Page', () =>{
    expect(testQ4()).toMatch("4. What went well? How can I recreate this?\n" + "Finish everything that is due on Friday.\n");
})

/*
    Test Question 5 on Prepare page
*/
function testQ5(){
    var question = "5. Any other lessons learnt?\n";
    var answer = "Planning out the day may help to be more organized.\n";
    return fullSentence(question, answer);
}
test('TEST Question 5 on Prepare Page', () =>{
    expect(testQ5()).toMatch("5. Any other lessons learnt?\n" +  "Planning out the day may help to be more organized.\n");
})
