/* Listing all tests below... */
/* Done By: Kevin Su */

/* Test 0: INIT
           Upon executing the init, the local storage variables
           should be allocated correctly (as string...)*/

function init1(){
    localStorage["a"] = 10;
    localStorage["b"] = 15;
    localStorage["c"] = 20;
    return [localStorage["a"], localStorage["b"], localStorage["c"]]
}

test('Test 0: INIT variables', () => {
    expect(init1()).toStrictEqual(["10","15","20"]);
})

/* Test 1: The - button
           Upon clicking the - button the local storage variable
           for "work" and "break" should be decremented by 5 and
           the local storage variable for "cycles" should be
           decremented by 1*/

function init2(){
    localStorage["breaktest"] = 10;
    localStorage["worktest"] = 20;
    localStorage["cycletest"] = 3;

    /* Apply the functions to each local storage*/
    minus("worktest");
    minus("breaktest");
    minus("cycletest");

    return [localStorage["breaktest"], localStorage["worktest"], localStorage["cycletest"]];
}

/* Working same as - button */
function minus(storage_string){
    if (storage_string == "breaktest" || storage_string == "worktest"){
        if(localStorage[storage_string] != "5"){
            localStorage[storage_string] = parseInt(localStorage[storage_string]) - 5;
        }
    }

    if (storage_string == "cycletest"){
        if(localStorage[storage_string] != "1"){
            localStorage[storage_string] = parseInt(localStorage[storage_string]) - 1;
        }
    }
}

test('Test 1: decrementing storage variables', () => {
    expect(init2()).toStrictEqual(["5","15","2"]);
})

/* Test 2: The + button
           Upon clicking the + button the local storage variable
           for "work" and "break" should be incremented by 5 and
           the local storage variable for "cycle" should be
           incremented by 1*/

function init3(){
    /* Assemble test variables */
    localStorage["breaktest"] = 10;
    localStorage["worktest"] = 20;
    localStorage["cycletest"] = 3;

    /* Apply the functions to each local storage*/
    plus("worktest");
    plus("breaktest");
    plus("cycletest");

    return [localStorage["breaktest"], localStorage["worktest"], localStorage["cycletest"]];
}

/* A function working the same as the + button */
function plus(storage_string){
    if (storage_string == "breaktest" || storage_string == "worktest"){
        if(localStorage[storage_string] != "60"){
            localStorage[storage_string] = parseInt(localStorage[storage_string]) + 5;
        }
    }

    if (storage_string == "cycletest"){
        if(localStorage[storage_string] != "4"){
            localStorage[storage_string] = parseInt(localStorage[storage_string]) + 1;
        }
    }
}

test('Test 2: incrementing storage variables', () => {
    expect(init3()).toStrictEqual(["15","25","4"]);
})

/* Test 3: Upper bound on work
           The + button should have no effect if the "work" value
           is currently at 60*/

function init4(){
    localStorage["worktest"] = 60;
    plus("worktest");
    return localStorage["worktest"];
}

test('Test 3: max limit work variable', () => {
    expect(init4()).toStrictEqual("60");
})

/* Test 4: Lower bound on work
           The - button should have no effect if the "work" value
           is currently at 5*/

function init5(){
    localStorage["worktest"] = 5;
    minus("worktest");
    return localStorage["worktest"];
}

test('Test 4: min limit work variable', () => {
    expect(init5()).toStrictEqual("5");
})

/* Test 5: Upper bound on break
           The + button should have no effect if the "break" value
           is currently at 60*/

function init6(){
    localStorage["breaktest"] = 60;
    plus("breaktest");
    return localStorage["breaktest"];
}

test('Test 5: max limit break variable', () => {
    expect(init6()).toStrictEqual("60");
})

/* Test 6: Lower bound on break
           The - button should have no effect if the "break" value
           is currently at 5*/

function init7(){
    localStorage["breaktest"] = 5;
    minus("breaktest");
    return localStorage["breaktest"];
}

test('Test 6: min limit break variable', () => {
    expect(init7()).toStrictEqual("5");
})

/* Test 7: Upper bound on number of cycles
           The + button should have no effect if the "cycle" value
           is currently at 4*/

function init8(){
    localStorage["cycletest"] = 4;
    plus("cycletest");
    return localStorage["cycletest"];
}

test('Test 7: max limit cycle variable', () => {
    expect(init8()).toStrictEqual("4");
})

/* Test 8: Lower bound on number of cycles
           The - button should have no effect if the "cycle" value
           is currently at 1*/

function init9(){
    localStorage["cycletest"] = 1;
    minus("cycletest");
    return localStorage["cycletest"];
}

test('Test 8: min limit cycle variable', () => {
    expect(init9()).toStrictEqual("1");
})

/* Test 9: Random assortment of plus and minus
           Test various effects of + and - buttons */

function init10(){
    localStorage["cycletest"] = 1;
    localStorage["breaktest"] = 50;
    localStorage["worktest"] = 55;
    minus("cycletest");
    plus("worktest");
    plus("breaktest");
    return [localStorage["breaktest"], localStorage["worktest"], localStorage["cycletest"]];
}

test('Test 9: one + and - on each variable', () => {
    expect(init10()).toStrictEqual(["55", "60", "1"]);
})

/* Test 10: Multiple + and - on same variable
            The + and - buttons should work when stacked
            up on the same variables */

function init11(){
    localStorage["cycletest"] = 1;
    localStorage["breaktest"] = 5;
    localStorage["worktest"] = 25;

    minus("cycletest");
    plus("cycletest");
    plus("cycletest");

    plus("worktest");
    plus("worktest");
    plus("worktest");
    plus("worktest");
    plus("worktest");
    plus("worktest");
    plus("worktest");
    minus("worktest");
    minus("worktest");
    plus("worktest");

    plus("breaktest");
    plus("breaktest");
    plus("breaktest");
    return [localStorage["breaktest"], localStorage["worktest"], localStorage["cycletest"]];
}

test('Test 10: multiple + and - on each variable', () => {
    expect(init11()).toStrictEqual(["20", "55", "3"]);
})

/* Test 11: Total Time
            The total time should be equal to the number of cycles
            times the work time*/

function init12(){
    /* 1000 seconds */
    localStorage["cycletest"] = 4;
    localStorage["worktest"] = 250;

    var total_time = parseInt(localStorage["cycletest"]) * parseInt(localStorage["worktest"]);
    return total_time;
}

test('Test 11: total time should sum correctly', () => {
    expect(init12()).toStrictEqual(1000);
})

/* Test 12: Time in seconds to hh:mm:ss format
            The total time should format correctly as hh:mm:ss*/

function init13(){
    localStorage["cycletest"] = 4;
    localStorage["worktest"] = 250;

    var total_time = parseInt(localStorage["cycletest"]) * parseInt(localStorage["worktest"]);
    var final_time = time_format(total_time);
    return final_time;
}

function time_format(total_time){
    var hours = Math.floor(total_time/3600);
    if(hours < 10){
        var str_hours = "0" + hours.toString();
    }else{
        var str_hours = hours.toString();
    }
    total_time = total_time % 3600;
    var minutes = Math.floor(total_time/60);
    if(minutes < 10){
        var str_minutes = "0" + minutes.toString();
    }else{
        var str_minutes = minutes.toString();
    }
    var seconds = total_time % 60;
    if(seconds < 10){
        var str_seconds = "0" + seconds.toString();
    }else{
        var str_seconds = seconds.toString();
    }

    var total = str_hours + ":" + str_minutes + ":" + str_seconds;
    return total;
}

test('Test 12: Total time should be in format hh:mm:ss (basic)', () => {
    expect(init13()).toStrictEqual("00:16:40");
})

/* Test 13: Time in seconds to hh:mm:ss format
            The total time should format correctly as hh:mm:ss
            More complex test...*/

function init14(){
    localStorage["cycletest"] = 3;
    localStorage["worktest"] = 16000;

    var total_time = parseInt(localStorage["cycletest"]) * parseInt(localStorage["worktest"]);
    var final_time = time_format(total_time);
    return final_time;
}

test('Test 13: Total time should be in format hh:mm:ss (complex)', () => {
    expect(init14()).toStrictEqual("13:20:00");
})

/* Test 14: Decrementing time in hh:mm:ss format
            The total time should format correctly as hh:mm:ss*/

function init15(){

    /* Should be 1 hours */
    localStorage["cycletest"] = 3;
    localStorage["worktest"] = 1200;

    /* Test 1 second has passed */
    localStorage["timePassed"] = 1;

    var total_time = parseInt(localStorage["cycletest"]) * parseInt(localStorage["worktest"]) - parseInt(localStorage["timePassed"]);
    var final_time = time_format(total_time);
    var str_time_passed = time_format(localStorage["timePassed"])
    return [final_time, str_time_passed];
}

test('Test 14: Decrementing time format hh:mm:ss (complex)', () => {
    expect(init15()).toStrictEqual(["00:59:59", "00:00:01"]);
})

/* Test 15: Clearly display the information to html
            DOM manipulating stuff...*/

function init16(){
    localStorage["worktest"] = 1200;
    document.getElementById("worktime").innerText = localStorage["worktest"];
}

test('Test 15: Correctly displays to html: DOM manipulating...', ()=>{
    document.body.innerHTML =
    '<div>' +
    '   <span id = "worktime"></span>' +
    '</div>';
    init16();
    expect(document.getElementById("worktime").innerText).toStrictEqual('1200');
})

/* Test 16: Clearly display the information to html
            DOM manipulating stuff...
            MORE COMPLEX*/

function init17(){
    localStorage["worktest"] = 50;
    localStorage["breaktest"] = 60;
    localStorage["cycletest"] = 3;
    document.getElementById("worktime").innerText = localStorage["worktest"];
    document.getElementById("breaktime").innerText = localStorage["breaktest"];
    document.getElementById("cycle").innerText = localStorage["cycletest"];

    var total_time = parseInt(localStorage["worktest"]) * parseInt(localStorage["cycletest"]);
    document.getElementById("total").innerText = total_time;
}

test('Test 16: Correctly displays to html: DOM manipulating... COMPLEX', ()=>{
    document.body.innerHTML =
    '<div>' +
    '   <span id = "worktime"></span>' +
    '   <span id = "breaktime"></span>' +
    '   <span id = "cycle"></span>' +
    '   <span id = "total"></span>' +
    '</div>';
    init17();

    expect(document.getElementById("worktime").innerText).toStrictEqual('50');
    expect(document.getElementById("breaktime").innerText).toStrictEqual('60');
    expect(document.getElementById("cycle").innerText).toStrictEqual('3');
    expect(document.getElementById("total").innerText).toStrictEqual(150);
})

/* Test 17: COMBINATION OF EVERYTHING... */

function init18(){
    localStorage["worktest"] = 50;
    localStorage["breaktest"] = 60;
    localStorage["cycletest"] = 3;

    plus("cycletest");
    minus("worktest");
    minus("breaktest");

    document.getElementById("worktime").innerText = localStorage["worktest"];
    document.getElementById("breaktime").innerText = localStorage["breaktest"];
    document.getElementById("cycle").innerText = localStorage["cycletest"];

    var total_time = parseInt(localStorage["worktest"]) * parseInt(localStorage["cycletest"]);
    document.getElementById("total").innerText = total_time;

    var total = time_format(total_time);
    document.getElementById("hh").innerText = total;
}

test('Test 17: Combination of all of the above...', ()=>{
    document.body.innerHTML =
    '<div>' +
    '   <span id = "worktime"></span>' +
    '   <span id = "breaktime"></span>' +
    '   <span id = "cycle"></span>' +
    '   <span id = "total"></span>' +
    '   <span id = "hh"></span>' +
    '</div>';
    init18();

    expect(document.getElementById("worktime").innerText).toStrictEqual('45');
    expect(document.getElementById("breaktime").innerText).toStrictEqual('55');
    expect(document.getElementById("cycle").innerText).toStrictEqual('4');
    expect(document.getElementById("total").innerText).toStrictEqual(180);
    expect(document.getElementById("hh").innerText).toStrictEqual('00:03:00');
})